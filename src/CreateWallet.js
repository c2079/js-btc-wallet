const fs = require('fs');
const bip32 = require('bip32');
const bip39 = require('bip39');
const bitcoin = require('bitcoinjs-lib');

const network = bitcoin.networks.testnet; // testing network
//const network = bitcoin.networks.bitcoin; // master bitcoin network

// derivate wallet 
const path = `m/49'/1'/0'/0`;

// create mnemonic
let mnemonic = bip39.generateMnemonic();

// Generate seed asynchronously and handle Promise
bip39.mnemonicToSeed(mnemonic).then(seed => {
    // root wallet
    const root = bip32.fromSeed(seed, network);

    // account
    const account = root.derivePath(path);
    let node = account.derive(0).derive(0);

    const btcAddress = bitcoin.payments.p2pkh({
        pubkey: node.publicKey,
        network: network,
    }).address;

    let message = `Wallet created\n\taddress: ${btcAddress}\n\tprivate key: ${node.toWIF()}\n\tseed: ${mnemonic}\n`;
    console.log(message);

    fs.writeFile('wallet.txt', message, error => {
        if (error) {
            console.error('Error writing to file: ', error);
            return;
        }

        console.log('Wallet created');
    });
}).catch(error => {
    console.error('Error generating seed: ', error);
});
